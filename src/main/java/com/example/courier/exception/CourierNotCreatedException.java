package com.example.courier.exception;

public class CourierNotCreatedException extends Exception{
    public CourierNotCreatedException (String message){
        super(message);
    }
}
